from django.urls import path

from . import views

app_name = 'restaurant'
urlpatterns = [
    path('', views.GeneralView.as_view(), name='general_view'),
    path('create/', views.RestaurantCreate.as_view(), name='create'),
    path('menu/create/', views.RestaurantMenuCreate.as_view(), name='menu_create'),
    path('<int:pk>/', views.RestaurantUpdate.as_view(), name='update'),
]
from django.http import request
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView
from .models import Restaurant, RestaurantMenuType


class GeneralView(generic.ListView):
    template_name = 'restaurant/restaurant_list.html'
    context_object_name = 'restaurant_list'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            restaurant_list = Restaurant.objects.filter(name__icontains=query)
        else:
            restaurant_list = Restaurant.objects.all()
        return restaurant_list

class RestaurantMenuCreate(CreateView):
    model = RestaurantMenuType
    fields = '__all__'
    template_name = 'restaurant/restaurant_menu_form.html'
    success_url = '/restaurant/create/'

class RestaurantCreate(CreateView):
    class Meta:
        model = RestaurantMenuType
        fields = '__all__'
    model = Restaurant
    fields = '__all__'
    success_url = '/restaurant/'


class RestaurantUpdate(UpdateView):
    class Meta:
        model = RestaurantMenuType
        fields = '__all__'
    model = Restaurant
    fields = '__all__'
    template_name_suffix = '_update_form'
    success_url = '/restaurant/'
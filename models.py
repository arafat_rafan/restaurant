from enum import Enum

from django.db import models

class RestaurantMenuType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Restaurant(models.Model):
    class CATEGORY(Enum):
        FASTFOOD = ('FASTFOOD', 'Focus is primarily on quick service')
        FAMILYSTYLE = ('FAMILYSTYLE', 'Food served on large platters for parties to share')
        FINDINING = ('FINDINING', 'Formal dress code and fine dining etiquette')
        CASUALDINING = ('CASUALDINING', 'Moderately-priced menus')
        CAFE = ('CAFE', 'Usually serve coffee, tea, pastries, and small items for breakfast and lunch')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]
    menu = models.ForeignKey(RestaurantMenuType, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    category = models.CharField(
        max_length=150,
        choices=[x.value for x in CATEGORY],
        default=CATEGORY.get_value('FASTFOOD'),
    )
    price_range = models.CharField(max_length=25, blank=True)
    buffet = models.BooleanField(default=False)
    smoking_room = models.NullBooleanField(default=False)

    def __str__(self):
        return self.name
